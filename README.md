# dotfiles

This repo stores dot files to bootstrap my GNU/Linux development machine.

I going to start saving files for three of most important tools:

* bash: ~/.bashrc, ~/.bash_local, ~/.bash_aliases
* vim: ~/.vimrc
* screen: ~/.screenrc
* terminator: ~/.config/terminator/config

## References

https://dotfiles.github.io/
